<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Blog extends Migration
{
    /**
     * Рука тянется вынести в Models/Blog (не люблю реализацию размазывать по всему проекту), но тогда вся идея
     * миграций в ларе пойдет прахом.
     *
     * Как синхронизировать миграции с протоколом общения с бд - не разобрался. Откатим мы миграцию, а у нас обвалятся
     * все контроллеры, потому что сдохла колонка.
     */
    public function up()
    {
        Schema::create( 'blog', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->text( 'title' );
            $table->text( 'text' );
        } );
    }

    public function down() {}
}