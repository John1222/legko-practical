<?php
use App\Models\Blog;
use Illuminate\Database\Seeder;

/**
 * Не понял, как обстоит дело с синхронизацией миграцией с сидами. Видимо, никак.
 */
class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Blog::insertTestData( 10 );
    }
}