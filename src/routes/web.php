<?php
Route::get( '/', 'BlogController@index' );
Route::get( '/api/posts/get_all', 'BlogController@getPostsAsJson' );
Route::get( '/api/posts/get/{id}', 'BlogController@getPostByIdAsJson' );
Route::get( '/api/posts/delete/{id}', 'BlogController@deletePostById' );