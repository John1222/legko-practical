require('./bootstrap');
let angular = require( 'angular' );

let app = angular.module( "BlogApp", [ 'vesparny.fancyModal' ] );
(function( app ) {
    app.controller( "PostsController", function( $scope, $http, $fancyModal, $location ) {
        $scope.sortByDesc = true;
        $scope.propertyName = 'id';
        $scope.videos = [
            'AtFiJpRJwKs',
            '6kiIVZqssO8',
            'x7Zq-7fLieQ',
            'g4clTFuYOXQ',
            'xb9zyQfoDbQ',
            '9CpNpPZHnNQ',
            'osqRZsB8IYQ',
            'mwKecF0osZs',
            'dH7XIejR7KE',
            'CPy_fg1YqJM'
        ];
        $scope.post = {};

        $scope.sortBy = function( propertyName ) {
            $scope.sortByDesc = ($scope.propertyName === propertyName) ? !$scope.sortByDesc : false;
            $scope.propertyName = propertyName;
            $location.url( '/?propertyName=' + $scope.propertyName + '&sortByDesc=' + $scope.sortByDesc );
        };

        $scope.deletePost = function( postId ) {
            // TODO обрабатывать ответ
            $http.get( "/api/posts/delete/" + postId )
                .then( function() {
                    location.reload();
                } );
        };

        $scope.showRandomVideo = function() {
            let videoId = $scope.videos[ Math.floor( Math.random() * $scope.videos.length ) ];
            $fancyModal.open( {
                themeClass: 'fancymodal-theme-custom',
                template: '<iframe width="560" height="315" src="https://www.youtube.com/embed/' + videoId + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
            } );
        };

        $scope.showPostText = function( postId ) {
            $location.url( '/?post=' + postId );
            $http.get( "/api/posts/get/" + postId )
                .then( function( responseJson ) {
                    // TODO темплейт бы вынести в templateUrl, но при попытке подключить контроллер fancy зацикливает окно
                    // TODO нужно бы сбрасывать урл при закрытии окна, но я не нашел, где зацепиться за евент. Вроде result должен быть, но его нет
                    let modal = $fancyModal.open( {
                        template: '<div>\n' +
                            '    <h3>' + responseJson.data[ 0 ].title + '</h3>\n' +
                            '    <hr>' + responseJson.data[ 0 ].text + '\n' +
                            '</div>'
                    } );
                } );
        };

        // TODO вынести нижеследующие блоки в отдельные методы
        $http.get( "/api/posts/get_all" )
            .then( function( res ) {
                $scope.posts = res.data;
            } );

        let search = $location.search();
        if( search.propertyName !== undefined ) {
            $scope.propertyName = search.propertyName;
            $scope.sortByDesc = (search.sortByDesc === 'true');
        }
        else if( 0 < search.post ) {
            $scope.showPostText( search.post );
        }
    } );
})( app );