<html ng-app="BlogApp">
<head>
    <link rel="stylesheet" href="/public/css/app.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css"/>

    <script src="/public/js/all.js"></script>
</head>
<body ng-controller="PostsController">

<h2>Записи в блоге</h2>

<div class="Rtable">
    <div class="Rtable-cell-id" ng-click="sortBy( 'id' )">
        <h3>
            ID
            <span class="sortorder" ng-show="propertyName === 'id'" ng-class="{sortByDesc: sortByDesc}"></span>
        </h3>
    </div>
    <div class="Rtable-cell-title" ng-click="sortBy( 'title' )">
        <h3>
            Заголовок
            <span class="sortorder" ng-show="propertyName === 'title'" class="sortorder"
                  ng-class="{sortByDesc: sortByDesc}"></span>
        </h3>
    </div>
    <div class="Rtable-cell-op"><h3></h3></div>

    <div class="Rtable-cell-id" ng-repeat-start="post in posts | orderBy:propertyName:sortByDesc">
        @{{post.id}}
    </div>
    <div class="Rtable-cell-title">
        <a href="javascript:void(0)" ng-click="showPostText( post.id )">
            @{{post.title}}
        </a>
    </div>
    <div class="Rtable-cell-op" ng-repeat-end>
        <a href="#" ng-click="showRandomVideo()" title="Показать видео"><img src="/public/img/video.png"></a>
        <a href="#" ng-click="deletePost( post.id )" title="Удалить запись"><img src="/public/img/delete.png"></a>
    </div>
</div>

</body>
</html>