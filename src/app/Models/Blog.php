<?php
namespace App\Models;

use DB;

/**
 * Да, я знаю о существовании Model. Вот только контактирование с интерфейсом посредством магических переменных - 
 * это даже больше, чем просто быдлокод. Наверняка кулибины для эстетов вроде вашего покорного слуги уже сообразили
 * более кошерную реализацию, но мне пока столько не платят.
 */
class Blog
{
    private static $table = 'blog';

    public static function insertTestData( int $amount )
    {
        for( $i = 0; $i < $amount; ++$i ) {
            $referat = YandexReferats::getRandom();
            if( $referat != null ) {
                DB::table( self::$table )->insert( [
                    'title' => $referat->getTitle(),
                    'text' => $referat->getText()
                ] );
            }
        }
    }

    /**
     * @param int $id
     * /**
     * @return \Illuminate\Support\Collection
     */
    public static function getPostById( int $id )
    {
        return DB::table( self::$table )->where( 'id', $id )->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getAllPosts()
    {
        return DB::table( self::$table )->get();
    }

    /**
     * @param int $postId
     * @return bool
     */
    public static function deletePostById( int $postId )
    {
        return 0 < DB::table( self::$table )->where( 'id', $postId )->delete();
    }
}

class YandexReferats
{
    /**
     * @return YandexReferat|null
     */
    public static function getRandom()
    {
        for( $requestAttempt = 0; $requestAttempt < 5; ++$requestAttempt ) {
            $referatRaw = file_get_contents( 'https://yandex.ru/referats/?t=astronomy+geology+gyroscope+literature+marketing+mathematics+music+polit+agrobiologia+law+psychology+geography+physics+philosophy+chemistry+estetica' );
            if( $referatRaw === false ) {
                echo 'CONTINUE';
                continue;
            }
            preg_match( '/<div class="referats__text">[\s\S]*<strong>.*?«(.*?)»<\/strong>([\s\S]*?)<div><button/',
                $referatRaw,
                $m );
            $title = $m[ 1 ];
            $text = $m[ 2 ];
            return new YandexReferat( $title, $text );
        }
        
        return null;
    }
}

class YandexReferat
{
    private $title;
    private $text;
    
    public function __construct( string $title, string $text )
    {
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }
}

/*
class BlogPost
{
    private $id;
    private $title;
    private $text;
    
    public function __construct( int $id, string $title, string $text )
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getText()
    {
        return $this->text;
    }
}
*/