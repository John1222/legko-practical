<?php
namespace App\Http\Controllers;

use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
        return view( 'welcome' );
    }

    /**
     * @return string
     */
    public static function getPostsAsJson()
    {
        return Blog::getAllPosts()->toJson();
    }

    /**
     * @param int $postId
     * @return string
     */
    public static function getPostByIdAsJson( int $postId )
    {
        return Blog::getPostById( $postId )->toJson();
    }
    
    public static function deletePostById( int $postId )
    {
        Blog::deletePostById( $postId );
    }
}