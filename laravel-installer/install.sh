#!/usr/bin/env bash

apk update

# Распаковываем
composer install
./vendor/bin/laravel new

# Ставим зависимости
composer install
npm install
npm install gulp-cli -g

# Конфим
php artisan key:generate
mv ./server.php ./index.php