#!/usr/bin/env bash

WWW_DIR='/var/www'
rm -R ${WWW_DIR}
mkdir -p ${WWW_DIR}
cd ${WWW_DIR}

# Docker
sudo apt-get update
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
sudo apt-get update
sudo apt-get install docker-engine -y
sudo docker network create external-net

# Docker Compose
curl -L https://github.com/docker/compose/releases/download/1.23.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# git
sudo apt-get install git

# Качаем репо
sudo git clone https://brocoder1@bitbucket.org/brocoder_team/test-job-3.git .

# Все бинари должны быть executable, ибо нехуй
chmod -R +x ./bin

# Чистим прошлый билд
rm -R ./~laravel
rm -R ./~db

# Рестартим конты
docker rm -f $(docker ps -a -q)
docker-compose up -d

# Инсталлим лару
cp -R ./laravel-installer/. ./~laravel/
docker exec -it laravel bash ./install.sh
chmod -R 777 ./~laravel/storage

# Ставим наши сорсы
cp -a ./src/. ./~laravel/

# Ставим наши npm-зависимости (package-src.json)
docker exec -it laravel bash ./package-install.sh

# Компилим получившееся
docker exec -it laravel gulp

# Засираем БД говноданными
bash ./bin/seed-db.sh