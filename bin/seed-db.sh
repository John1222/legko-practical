#!/usr/bin/env bash

docker exec -it laravel php artisan migrate
docker exec -it laravel php artisan db:seed